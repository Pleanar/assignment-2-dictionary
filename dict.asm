%include "lib.inc"
%include "colon.inc"

section .text

global find_word

; Принимает два аргумента на вход:
; Указатель на нуль-терминированную строку (ключ) в rdi,
; Указатель на начало словаря в rsi.
; Ищет в словаре походящий ключ.
; Если ключ найден, то возвращает в rax адрес начала вхождения значения в словарь.
; Если ключ не найден, то возвращает в rax 0.

find_word:
	push rdi
	push rsi
	test rsi, rsi
	jz .no_key
	.loop:
		mov rdi, qword [rsp+8]
		add rsi, key_adress_offset
		call string_equals
		test rax, rax
		jnz .found_key
		pop rsi
		mov rsi, qword [rsi]
		push rsi
		test rsi, rsi
		jnz .loop
	.no_key:
		add rsp, 16
		xor rax, rax
		ret
	.found_key:
		mov rdi, qword [rsp+8]
		call string_length
		pop rsi
		add rsi, key_adress_offset
		add rax, 1
		add rax, rsi
		add rsp, 8
		ret
