%include "words.inc"
%include "dict.inc"
%include "lib.inc"

%xdefine buf_size 256
%xdefine enter_symbol 0xA
%xdefine null_symbol 0x0


section .rodata
f_error:
    db "Error: the line doesn't match any key word in dictionary.", enter_symbol, null_symbol
r_error:
    db "Error: the line was not read fully due to buffer overflow.", enter_symbol , null_symbol

section .bss
buffer: resb buf_size

section .text
global _start

_start:
	mov rdi, buffer
	mov rsi, buf_size
	call read_line
	test rax, rax
	jz .read_error
	mov rdi, rax
	mov rsi, last_label
	call find_word
	test rax, rax
	jz .find_error
	push rax
	call print_newline
	pop rdi
	call print_string
	call print_newline
	xor rdi, rdi
	call exit
	.read_error:
		call print_newline
		mov rdi, r_error
		call print_error
		mov rdi, 1
		call exit
	.find_error:
		call print_newline
		mov rdi, f_error
		call print_error
		mov rdi, 2
		call exit
