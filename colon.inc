%ifndef COLON_INC
%define COLON_INC

%define last_label 0x0
%xdefine key_adress_offset 8

%macro colon 2
	%ifstr %1
		%ifid %2
			%2:
				dq last_label
				db %1, 0x0
			%define last_label %2
		%else
			%error "Second argument should be a label"
		%endif
	%else
		%error "First argument should be a string"
	%endif
%endmacro

%endif
