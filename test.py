#!/usr/bin/env python2.7

import subprocess

stdin = ["first_word", "second_word", "third_word", "first word", "Ecghewmhsnileuvnhgsueilcgninhlgiwehncilgwhercnigweurh goiruvh  lriwugh3gl iuh3ulhvu3h giudhwgl3wg983muqc84 29y94y4297y7c2yh3o27y g39uh g 982 3y229hfo9 734y9gh234o7g2yog9 u2h4o3 u9gy923h giuho2o4lohv ufvrou237hf viu hf4h3qoaewghmgfwhghacehhaflhb alweh bf whbehcbwe,ha bgmauchbmawhvebj hcaewghmgfwhghacehhaflhb alweh bf whbehcbwe,ha bgmauchbmawhvebj hcaewghmgfwhghacehhaflhb alweh bf whbehcbwe,ha bgmauchbmawhvebj hcaewghmgfwhghaceh"]
expected_stdout = ["first word explanation", "second word explanation", "third word explanation", "", ""]
expected_stderr = ["", "", "", "Error: the line doesn't match any key word in dictionary.", "Error: the line was not read fully due to buffer overflow."]
flag = True

print("Running tests ...\n")
for i in range(len(stdin)):
    lab2 = subprocess.Popen(["./lab2"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = lab2.communicate(input=stdin[i])
    stdout = stdout.strip()
    stderr = stderr.strip()
    if stdout == expected_stdout[i] and stderr == expected_stderr[i]:
	print("---------------------")
        print("Test " + str(i + 1) + ": passed")
	print("---------------------")
    elif stdout != expected_stdout[i]:
        flag = False
	print("---------------------")
        print("Test " + str(i + 1) + ": failed")
        print("Got in stdout: " + stdout + ";\nExpected: " + expected_stdout[i] + ".")
	print("---------------------")
    else:
        flag = False
	print("---------------------")
        print("Test " + str(i + 1) + ": failed")
        print("Got in stderr: " + stderr + ";\nExpected: " + expected_stderr[i])
	print("---------------------")
if flag:
	print("---------------------")
	print("All tests are passed")
	print("---------------------")
