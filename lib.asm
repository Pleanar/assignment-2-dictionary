section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_line
global print_error

%xdefine space_symbol 0x20
%xdefine tab_symbol 0x9
%xdefine enter_symbol 0xA
%xdefine null_symbol 0x0

%xdefine exit_code 60
%xdefine write_code 1
%xdefine stdout 1
%xdefine stderr 2

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, exit_code
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
     mov  rax, rdi
    .counter:
        cmp  byte [rdi], null_symbol
        je   .end
        inc  rdi
        jmp  .counter
    .end:
        sub  rdi, rax
        mov  rax, rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, stdout
    call write_str_to
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, write_code
    mov rdi, stdout
    mov rdx, 1
    syscall
    pop rax
    xor rax, rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, enter_symbol
    call print_char
    xor rax, rax
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
print_uint:
    mov rax, rdi
    mov rdi, 10
    xor rcx, rcx
    xor rdx, rdx

    .loop:
        div rdi
        add rdx, "0"
        push rdx
        xor rdx, rdx
        inc rcx
        test rax, rax
        jnz .loop

    .print:
        pop rdi
        push rcx
        call print_char
        pop rcx
        dec rcx
        jnz .print

    xor rax, rax
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, "-"
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
        mov al, byte [rdi]
        cmp al, byte [rsi]
        jne .error
        cmp byte [rdi], null_symbol
        je .end
        inc rdi
        inc rsi
        jmp .loop
    .error:
        xor rax, rax
        ret
    .end:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера в rdi, размер буфера в rsi
; Читает в буфер слово из stdin, пропуская пробельные символы в начале.
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера.
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax.
; Эта функция должна дописывать к слову нуль-терминатор.

read_word:
    push rdi
    push rdi
    push rsi

    .skip_loop:
        call read_char
        test rax, rax
        jz .end
        cmp rax, space_symbol
        je .skip_loop
        cmp rax, tab_symbol
        je .skip_loop
        cmp rax, enter_symbol
        je .skip_loop
        jmp .after_skip_loop

    .read_loop:
        call read_char
    .after_skip_loop:
        test rax, rax
        jz .end
        cmp rax, space_symbol
        je .end
        cmp rax, tab_symbol
        je .end
        cmp rax, enter_symbol
        je .end
        cmp qword [rsp], 0
        je .error
        mov rsi, [rsp+8]
        mov byte [rsi], al
        dec qword [rsp]
        inc qword [rsp+8]
        jmp .read_loop

    .end:
        pop rcx
        pop rsi
        xor rdi, rdi
        mov [rsi], rdi
        pop rdi
        mov rax, rdi
        sub rsi, rdi
        mov rdx, rsi
        ret

    .error:
        add rsp, 24
        xor rax, rax
        xor rdx, rdx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rdi
    xor rax, rax
    mov r10, 10

    .counter:
        cmp byte [rdi], "0"
        jb  .end
        cmp byte [rdi], "9"
        ja  .end
        mul r10
        push rax
        xor rax, rax
        mov al, byte [rdi]
        sub rax, "0"
        add rax, [rsp]
        pop r9
        inc rdi
        jmp .counter
    .end:
        pop rdx
        sub rdi, rdx
        mov rdx, rdi
        xor rdi, rdi
        xor r10, r10
        xor r9, r9
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], "-"
    jne parse_uint

    .negative:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    dec rdx
    cmp rdx, rax
    jl .error
    xor rcx, rcx
    pop rsi
    pop rdi
    .loop:
        cmp rcx, rax
        je .return
        push rax
        xor rax, rax
        mov al, byte [rdi+rcx]
        mov byte [rsi+rcx], al
        pop rax
        inc rcx
        jmp .loop
    .error:
        add rsp, 16
        xor rax, rax
        ret
    .return:
        mov byte [rsi+rcx], 0
        ret

; Принимает на вход два аргумента
; В rdi принимает адрес начала буфера
; В rsi принимает длину буфера
; Записывает в буфер нуль-терминированную строку не более [размера буфера - 1]
; Если запись удалась, возвращает в rax адрес начала буфера
; Если запись не удалась (строка длинее максимума буфера), возвращает в rax 0
read_line:
	push rdi
	push rdi
	push rsi
	.loop:
		cmp qword [rsp], null_symbol
		je .error
		call read_char
		dec qword [rsp]
		mov rdi, qword [rsp+8]
		mov byte [rdi], al
		inc qword [rsp+8]
		test rax, rax
		jnz .loop
	.end:
		add rsp, 16
		pop rax
		ret
	.error:
		add rsp, 24
		xor rax, rax
		ret

; Принимает в rdi адрес нуль-терменированной строки, выводит её в stderr.
print_error:
    mov rsi, stderr
    call write_str_to
    ret

; Принимает на вход адрес нуль-терминированной строки в rdi, выход в rsi.
; Возвращает 0 в rax.
write_str_to:
	push rdi
	push rsi
	call string_length
	mov rdx, rax
	pop rdi
	pop rsi
	mov rax, write_code
	syscall
	xor rax, rax
	ret
