.PHONY: clean, test, run

ASM=nasm
ASMFLAGS=-f elf64 -o
LD=ld -o

PROJECT_NAME=lab2

all: $(PROJECT_NAME) test clean

clean:
	rm *.o lab2

%.o: %.asm
	$(ASM) $(ASMFLAGS) $@ $<

$(PROJECT_NAME): lib.o dict.o main.o
	$(LD) $@ $^

test:
	python2.7 ./test.py

run: $(PROJECT_NAME)
	./$(PROJECT_NAME)
